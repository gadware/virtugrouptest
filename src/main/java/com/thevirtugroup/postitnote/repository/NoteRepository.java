
package com.thevirtugroup.postitnote.repository;


import java.util.ArrayList;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.thevirtugroup.postitnote.model.Note;

@Repository
public class NoteRepository {

    private ArrayList<Note> allNotes;

    public ArrayList<Note> getNotes(String userName){
        return new ArrayList<Note>(allNotes.stream().filter(note -> note.getUserName()==userName).collect(Collectors.toList()));
    }

    public void saveNote(Note newNote){
    	allNotes.add(newNote);
    }
    
    public void deleteNote(String userName, String noteName){
    	allNotes = new ArrayList<Note>(allNotes.stream().filter(note -> note.getUserName()!=userName && note.getNoteName()!=noteName).collect(Collectors.toList()));
    }

}
