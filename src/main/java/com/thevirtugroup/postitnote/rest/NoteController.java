package com.thevirtugroup.postitnote.rest;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.service.NoteService;

/**
 */
@RestController
public class NoteController
{
	
	@Autowired
	private NoteService noteService;
	
	
	@PostMapping("/api/getnotes")
	public ArrayList<Note> getAllNotes(@RequestBody String userName) {
		return noteService.getAllNotes(userName);
	}
	
	@PostMapping("/api/savenote")
	public void addUser(@RequestBody Note note) {
		noteService.saveNote(note);
	}
	
	
	@DeleteMapping("/api/deletenote/{userName}{noteName}")
	public void deletNote(@PathVariable(name = "userName") String userName, @PathVariable(name = "noteName") String noteName) {
		noteService.deleteNote(userName, noteName);
	}

}
