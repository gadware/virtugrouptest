package com.thevirtugroup.postitnote.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;
import com.thevirtugroup.postitnote.service.NoteService;

@Service
public class NoteServiceImpl implements NoteService {

	
	@Autowired
	private NoteRepository noteRepository;
	

	@Override
	public ArrayList<Note> getAllNotes(String userName) {
		return noteRepository.getNotes(userName);
	}

	@Override
	public void saveNote(Note note) {
		noteRepository.saveNote(note);
	}
	
	@Override
	public void deleteNote(String userName, String noteName) {
		noteRepository.deleteNote(userName, noteName);
	}

}
