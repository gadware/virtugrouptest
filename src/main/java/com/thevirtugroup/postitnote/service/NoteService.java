package com.thevirtugroup.postitnote.service;

import java.util.ArrayList;

import com.thevirtugroup.postitnote.model.Note;

public interface NoteService {
	
	public ArrayList<Note> getAllNotes(String userName);
	
	public void saveNote(Note note);
	
	public void deleteNote(String userName, String noteName);

}
