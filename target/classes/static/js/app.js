(function(){

  var app = angular.module('notesApp',['ngRoute', 'ngMaterial']);
  var loggedUser = null;

  app.config(['$locationProvider', '$routeProvider',
      function ($locationProvider, $routeProvider) {

        $routeProvider
          .when('/', {
            templateUrl: '/partials/notes-view.html',
            controller: 'notesController'
          })
          .when('/login', {
             templateUrl: '/partials/login.html',
             controller: 'loginController',
          })
          .otherwise('/');
      }
  ]);

  app.run(['$rootScope', '$location', 'AuthService', function ($rootScope, $location, AuthService) {
      $rootScope.$on('$routeChangeStart', function (event) {

        $rootScope.logout = function(){
            $location.path("/");
        };

          if ($location.path() == "/login"){
             return;
          }

          if (!AuthService.isLoggedIn()) {
              console.log('DENY');
              event.preventDefault();
              $location.path('/login');
          }
      });
  }]);


  app.service('AuthService', function($http){
        

        function login (username, password){
            if (! username || !password){alert("Username and Password are required");}
            else
            {
            return $http.post("api/login", {username: username, password: password}).then(function(user){
                loggedUser = user;
            }, function(error){
                loggedUser = null;
                alert("Invalied username or password");
            })
            }
        }

        function isLoggedIn(){
            return loggedUser != null;
        }
        return {
            login : login,
            isLoggedIn: isLoggedIn
        }
  });

  app.controller('loginController', function($scope, AuthService, $location){

    $scope.invalidCreds = false;
    $scope.login = {
        username : null,
        password : null
    };

    $scope.login = function(){
        AuthService.login($scope.login.username, $scope.login.password).then(function(user){
            console.log(user);
            $location.path("/");
        }, function(error){
            console.log(error);
            $scope.invalidCreds = true;
        });
    };
  });




  app.service('NoteService', function($http){
    var allUserNotes = null;

    function loadNotes(){
        return $http.post("/api/getnotes", {username : loggedUser}).then(function(Notes){
            allUserNotes = Notes;
            console.log("notes loaded");
        }, function(error){
            allUserNotes = null;
            alert("No Note found");
        })
    }
});


  app.controller('notesController', function($scope, NoteService){

    $scope.isEditCreateView = false;

    $scope.cancelNoteView = function(){
        $scope.isEditCreateView = false;

    };

    $scope.newNoteView = function(){
        $scope.isEditCreateView = true;
    };

    $scope.deleteNote = function (i) {
      var r = confirm("Are you sure you want to delete this note?");
      if (r == true){
        $http.delete("api/deletenote", {username: username, password: password}).then(function(user){
            loggedUser = user;
        }, function(error){
            loggedUser = null;
            alert("Invalied username or password");
        })
      }
    };

    $scope.viewNote = function(){
        //TODO
    }
  });

})();